/**
 * Caribou Auxiliary Device header
 */

#ifndef CARIBOU_DEVICE_AUXILIARY_H
#define CARIBOU_DEVICE_AUXILIARY_H

#include "configuration.hpp"
#include "device.hpp"

#include <string>
#include <vector>

#include "auxiliarydevice.hcc"
#include "auxiliarydevice.tcc"

#endif /* CARIBOU_DEVICE_AUXILIARY_H */
